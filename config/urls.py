from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('kupci/', include('real_estate_api.kupci.urls')),
    path('korisnici/', include('real_estate_api.korisnici.urls')),
    path('stanovi/', include('real_estate_api.stanovi.urls')),
    path('ponude/', include('real_estate_api.ponude.urls')),

]
