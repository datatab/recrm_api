from django.test import TestCase
from django.urls import resolve
from rest_framework.test import APITestCase

from ..models import Kupci


class UserLoginTestCase(TestCase):
    def setUp(self):
        self.id_kupca = "1"
        self.Jmbg_Pib = 24234
        self.kupci = Kupci(id_kupca=self.id_kupca, Jmbg_Pib=self.Jmbg_Pib)

    def test_model_can_create_a_bucketlist(self):
        """Test the bucketlist model can create a bucketlist."""
        old_count = Kupci.objects.count()
        self.kupci.save()
        new_count = Kupci.objects.count()
        self.assertNotEqual(old_count, new_count)
