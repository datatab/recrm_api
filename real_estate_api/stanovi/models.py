from django.db import models
from django.urls import reverse
from django.utils import timezone

from real_estate_api.korisnici.models import Korisnici
from real_estate_api.kupci.models import Kupci


# from real_estate_api.stanovi.ponude_managers import PonudeStanovaKupcimaManager
# from real_estate_api.stanovi.stanovi_managers import StanoviManager


#####################################################################
#####################################################################
# #################### STANOVI #################################### #
#####################################################################
#####################################################################
class Stanovi(models.Model):
    class OrijentacijaStana(models.TextChoices):
        SEVER = 'Sever', "Sever"
        JUG = 'Jug', "Jug"
        ISTOK = 'Istok', "Istok"
        ZAPAD = 'Zapad', "Zapad"

    class StatusProdaje(models.TextChoices):
        DOSTUPAN = 'dostupan', "Dostupan"
        REZERVISAN = 'rezervisan', "Rezervisan"
        PRODAT = 'prodat', "Prodat"

    id_stana = models.BigAutoField(primary_key=True)
    lamela = models.CharField('Lamela', max_length=50, default=0)
    kvadratura = models.PositiveIntegerField('Kvadratura stana', default=0)
    sprat = models.PositiveIntegerField('Sprat stana', default=1)
    broj_soba = models.PositiveIntegerField('Broj soba stana', default=1)
    orijentisanost = models.CharField(max_length=20,
                                      choices=OrijentacijaStana.choices,
                                      default=OrijentacijaStana.SEVER,
                                      blank=False,
                                      null=False)
    broj_terasa = models.PositiveIntegerField('Broj terasa stana', default=0)
    cena_stana = models.PositiveIntegerField('Cena stana', default=0)
    cena_stana_za_kupca = models.PositiveIntegerField('Cena stana za kupca', default=0)
    napomena = models.CharField('Napomena', null=True, blank=True, max_length=250, default='')
    status_prodaje = models.CharField(max_length=20,
                                      choices=StatusProdaje.choices,
                                      default=StatusProdaje.DOSTUPAN)

    ''' null=True || zato sto kada se kreira stan ne znamo kom klijentu ce se dodeliti.
            models.SET_NULL || Jer kada se obrise iz sistema Klijent svi stanovi ostaju.'''
    klijent_prodaje = models.ForeignKey(Korisnici, null=True, blank=True, on_delete=models.SET_NULL)

    # objects = StanoviManager()
    def __repr__(self):
        return self.id_stana + ' je dodat.'

    def __str__(self):
        # return f"{self.id_stana} {self.cena_stana} {Kupci.objects.get(pk=self.id_stana)}"
        return f"{self.id_stana} {self.lamela} {self.kvadratura}  \
               {self.sprat} {self.broj_soba} {self.cena_stana} \
               {self.orijentisanost} {self.broj_terasa} {self.cena_stana} \
               {self.status_prodaje}"

    class Meta:
        db_table = 'stanovi'
        verbose_name = "Stan"
        verbose_name_plural = "Stanovi"

#####################################################################
#####################################################################
# #################### PONUDE ZA STAN ############################# #
#####################################################################
#####################################################################
# class PonudeStanovaKupcima(models.Model):
#     class StatusPonude(models.TextChoices):
#         POTENCIJALAN = 'potencijalan', "Potencijalan"
#         REZERVISAN = 'rezervisan', "Rezervisan"
#         KUPLJEN = 'kupljen', "Kupljen"
#
#     class NacinPlacanja(models.TextChoices):
#         U_CELOSTI = 'ceo_iznos', "Placanje u celosti"
#         KREDIT = 'kredit', "Kreditom"
#         RATE = 'na_rate', "Na rate"
#         UCESCE = 'ucesce', "Učešće"
#
#     kupac = models.ForeignKey(Kupci, on_delete=models.CASCADE)
#     stan = models.ForeignKey(Stanovi, on_delete=models.CASCADE)
#     cena_stana_za_kupca = models.PositiveIntegerField('Cena stana za kupca', default=0)
#     napomena = models.CharField(max_length=252, default="", blank=True)
#     broj_ugovora = models.CharField(max_length=252, default="", blank=True)
#     datum_ugovora = models.DateField(default=timezone.now)
#     status_ponude = models.CharField(max_length=20,
#                                      choices=StatusPonude.choices,
#                                      null=False, blank=False,
#                                      default=StatusPonude.POTENCIJALAN)
#     nacin_placanja = models.CharField(max_length=30,
#                                       choices=NacinPlacanja.choices,
#                                       null=False, blank=False,
#                                       default=NacinPlacanja.U_CELOSTI)
#
#     # Definisanje Q-a za izvestaje @see{real_estate_crm.stanovi.ponude_managers}
#     objects = PonudeStanovaKupcimaManager()
#
#     def __str__(self):
#         return f"{self.kupac.id_kupca} {self.cena_stana_za_kupca} \
#                   {self.broj_ugovora}"
#
#     def get_absolute_url(self):
#         return reverse('ponude', kwargs={'pk': self.pk})
#
#     class Meta:
#         db_table = 'ponude'
#         verbose_name = "Ponuda Kupcima"
#         verbose_name_plural = "Ponude Kupcima"
