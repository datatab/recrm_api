from rest_framework import serializers
from rest_framework.reverse import reverse

from real_estate_api.stanovi.views import Stanovi


class StanoviSerializer(serializers.ModelSerializer):
    #absolute_url = serializers.SerializerMethodField()

    class Meta:
        model = Stanovi
        fields = (
            "id_stana",
            "lamela",
            "kvadratura",
            "sprat",
            "broj_soba",
            "orijentisanost",
            "broj_terasa",
            "cena_stana",
            # "cena_stana_za_kupca",
            "napomena",
            "status_prodaje",
            "klijent_prodaje",
        )

        # TODO: Videti ovaj get_absolute_url
        # def get_absolute_url(self, obj):
        #     return reverse("lista_stanova")
